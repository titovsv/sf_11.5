FROM mariadb

WORKDIR /app

COPY ./script.sql .

EXPOSE 3306
